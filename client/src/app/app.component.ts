import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  show: boolean = false;


  @Input()  nome_cliente= '';
  @Input()  limite_credito = '';
  @Input()  tipo_risco = '';
  @Input()   taxa = '';
  @Input()  valor_final = '';
  datas:any = [];


  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    
    this.http.get('http://localhost:8080/cliente').subscribe(data => {
     this.datas = data;
    })
  } 


  ngOnChanges(changes: SimpleChanges){

    let value = changes.toString();
    if(value == "A"){
      this.taxa = '0';

      this.valor_final = this.limite_credito;
    }
    if(value == "B"){
      this.taxa = '10';
      if(parseFloat(this.limite_credito) > 0){
        let sum = (parseFloat(this.limite_credito) + (parseFloat(this.limite_credito) * 0.1));
        this.valor_final = sum.toString();
        console.log(this.valor_final)
      }
      
    }
    if(value == "C"){
      this.taxa = '20';
      if(parseFloat(this.limite_credito) > 0){
        let sum = (parseFloat(this.limite_credito) + (parseFloat(this.limite_credito) * 0.2));
        this.valor_final = sum.toString();
        
      }
    }
  }
  
  showFrom(){
    this.show = !this.show
    console.log(this.show)
  }
  onSubmit(event) { 
    event.preventDefault();

    const ParseHeaders = {
      headers: new HttpHeaders({
       'Content-Type'  : 'application/json'
      })
     };

     const data = {
      nome_cliente: this.nome_cliente,
      limite_credito: this.limite_credito,
      tipo_risco: this.tipo_risco,
      taxa: this.taxa,
      valor_final: this.valor_final
    }
    this.http.post('http://localhost:8080/cliente/', data , ParseHeaders) .subscribe(data => {
      if(data['id_cliente'] != null && data['id_cliente'] > 0){
        alert('Cliente Cadastrado com sucesso !');
        this.datas.push(data);
        this.show = false;
      }
    });
   }
  
  
}
