package com.trusthub.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.trusthub.entity.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente,String> {

}
