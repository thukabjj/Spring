package com.trusthub.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	@Column() 
	private Long id_cliente;
	@Column(nullable = false)
	private String nome_cliente;
	@Column(nullable = false) 
	private Double limite_credito;
	@Column(nullable = false)
	private String tipo_risco;
	@Column(nullable = false) 
	private Double taxa;
	@Column(nullable = false)
	private Double valor_final;
	
	public Cliente() {}
	
	public Cliente(String nome_cliente, Double limite_credito,String tipo_risco, Double taxa,Double valor_final) {
		
		this.nome_cliente = nome_cliente;
		this.limite_credito = limite_credito;
		this.tipo_risco  = tipo_risco;
		this.taxa  = taxa;
		this.valor_final = valor_final;
		
	}
	
	
	public Long getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(Long id_cliente) {
		this.id_cliente = id_cliente;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}
	public Double getLimite_credito() {
		return limite_credito;
	}
	public void setLimite_credito(Double limite_credito) {
		this.limite_credito = limite_credito;
	}
	public String getTipo_risco() {
		return tipo_risco;
	}
	public void setTipo_risco(String tipo_risco) {
		this.tipo_risco = tipo_risco;
	}
	public Double getTaxa() {
		return this.taxa;
	}
	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
	public Double getValor_final() {
		return valor_final;
	}
	public void setValor_final(Double valor_final) {
		this.valor_final = valor_final;
	}
	
}
