package com.trusthub;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.trusthub.entity.Cliente;
import com.trusthub.entity.repository.ClienteRepository;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteRepository repository;
	public ClienteController(ClienteRepository reposi) {
		this.repository = reposi;
	}
	
	
	@CrossOrigin
	@GetMapping
	@ResponseBody
	public Iterable<Cliente> clientes(){
		Iterable<Cliente> clientes = repository.findAll();
		return clientes;
		
	}
	@CrossOrigin
	@PostMapping
	@ResponseBody
	public Cliente create(@RequestBody @Valid Cliente cliente) {

		repository.save(cliente);
		return cliente;
	}
	
}
